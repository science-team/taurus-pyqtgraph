taurus-pyqtgraph (0.9.6-1) unstable; urgency=medium

  * d/watch: updated to use plain searchmode for gitlab
  * New upstream version 0.9.6
  * wrap-and-sort
  * Bug fix: "FTBFS: unsatisfiable build-dependencies: python3-pylsp (&lt;
    1.11~), python3-qtconsole (&lt; 5.6~), python3-spyder-kernels (&lt;
    2.6~)", thanks to Lucas Nussbaum (Closes: #1091042).

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 05 Mar 2025 08:28:01 +0100

taurus-pyqtgraph (0.8.0-1) unstable; urgency=medium

  * Add myself to Uploaders.
  * Switch to standard gbp+pristine-tar workflow (with a debian/watch file).
  * New upstream release.

 -- Roland Mas <lolando@debian.org>  Tue, 27 Feb 2024 14:12:20 +0100

taurus-pyqtgraph (0.6.2-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 0.6.2 (Closes: #1042327)
  * Drop pyqtgraph-0.13.patch
  * Bump Standards-Version to 4.6.2 (no changes needed)

 -- Nilesh Patra <nilesh@debian.org>  Thu, 12 Oct 2023 01:06:22 +0530

taurus-pyqtgraph (0.5.9-2) unstable; urgency=medium

  * Team upload.
  * d/gbp.conf: add.
    This is merely to document this gbp tree runs without pristine-tar
    branch.
  * pytest-pyqt5.patch: add.
    This addresses first part of #1030906.
    Thanks to s3v <c0llapsed@yahoo.it> for the hint
  * pyqtgraph-0.13.patch: add.
    This addresses second part of #1030906.
    Thanks to s3v <c0llapsed@yahoo.it> again (Closes: #1030906)

 -- Étienne Mollier <emollier@debian.org>  Mon, 27 Feb 2023 22:10:30 +0100

taurus-pyqtgraph (0.5.9-1) unstable; urgency=medium

  * New upstream version 0.5.9

 -- Carlos Pascual <cpascual@cells.es>  Wed, 17 Nov 2021 12:24:15 +0000

taurus-pyqtgraph (0.4.6-1) unstable; urgency=medium

  * New upstream version 0.4.6
  * bump compat from 12 to 13 (no changes needed)
  * bump standards version to 4.5.0 (no changes needed)
  * reenable tests after #939571 got fixed

 -- Carlos Pascual <cpascual@cells.es>  Fri, 25 Sep 2020 10:00:00 +0000

taurus-pyqtgraph (0.3.1-2) unstable; urgency=medium

  * source upload to unstable

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 16 Nov 2019 11:35:28 +0100

taurus-pyqtgraph (0.3.1-1) unstable; urgency=medium

  * Initial release (Closes: #933333)

 -- Carlos Pascual <cpascual@cells.es>  Tue, 15 Oct 2019 11:30:00 +0100
